

let deltaX = [0, 0, 0]; // ev.movementX is not reliable
let deltaY = [0, 0, 0];

const pieceData = {
	N: {
		head: "m 22.249805,3.160531 1.243335,10.803475 -9.943579,-4.487582 2.615861,12.016838 c -0.426922,0.610034 -0.79342,1.162814 -1.033529,1.559078 -0.307019,0.506688 -0.989106,1.996003 -1.504818,3.220992 -0.51571,1.224987 -0.865028,2.185501 -1.222664,3.291272 -0.715274,2.211542 -0.811395,3.313558 -1.4087,6.467822 -0.994997,5.254413 -1.4829867,17.483124 -1.6241897,21.557857 l -0.05736,0.10232 c 0,0 -0.0064,0.0084 -0.0062,1.724442 2.2e-4,1.716054 1.2499957,1.816992 2.4804687,2.725932 1.230475,0.908944 32.090764,0.266308 34.019112,0.125057 1.928341,-0.141253 1.933608,-0.163918 1.840198,-4.941817 -0.09341,-4.777898 -6.344971,-17.032756 -6.613033,-18.427816 -0.196197,-1.021057 5.250846,-3.247908 7.024377,-4.273124 1.633484,-0.630204 3.360457,-1.335705 4.829163,-1.977657 3.444447,-1.50552 4.982017,-3.331522 6.767027,-5.943824 1.22592,-1.794097 1.17894,-4.798909 0.72037,-7.159769 -0.24843,-1.278995 -1.36733,-3.468421 -2.08514,-4.402832 C 57.509392,14.124383 53.7659,10.895807 51.926546,10.152869 48.87575,8.920614 47.095059,8.227154 41.041423,8.052738 38.556141,7.981134 36.198751,8.437965 33.952966,9.160164 Z",
		body: "m 22.24",
		lefteye: [26.485, 18.631],
		righteye: [37.947, 13.921]
	},
	K: {
		head: "M 34.395833 0.38292236 A 0.78529021 1.4686842 0 0 0 33.747294 1.0226766 L 30.530436 9.8226562 A 1.9951489 3.7314151 0 0 0 30.71027 14.456482 L 32.717383 18.531685 A 2.2820899 4.2680648 0 0 0 36.074284 18.531685 L 38.127905 14.361397 A 1.9085217 3.5694011 0 0 0 38.299471 9.9291097 L 35.043856 1.0226766 A 0.78529021 1.4686842 0 0 0 34.395833 0.38292236 z M 26.569438 19.907829 A 12.483489 12.483489 0 0 0 14.085962 32.391305 A 12.483489 12.483489 0 0 0 14.123169 32.819702 L 14.098364 32.819185 C 14.122492 33.344943 14.179837 33.862063 14.259078 34.372579 A 12.483489 12.483489 0 0 0 14.30507 34.669718 C 15.843049 43.382431 25.177737 49.692025 34.215483 49.712211 C 43.881653 49.690611 54.086875 42.95684 54.530005 33.30081 L 54.474711 33.300293 A 12.483489 12.483489 0 0 0 54.554293 32.391305 A 12.483489 12.483489 0 0 0 42.070817 19.907829 A 12.483489 12.483489 0 0 0 34.339506 22.624976 A 12.483489 12.483489 0 0 0 26.569438 19.907829 z",
		body: "M 22.5538 48.2446 C 22.5765 48.2664 23.1828 48.762 23.9771 49.2457 C 27.0507 51.1171 30.6067 51.6414 34.3952 51.6414 C 38.7661 51.6414 42.8275 50.9435 46.1956 48.2842 L 46.2542 48.2277 C 46.5418 47.9505 46.941 47.8212 47.3365 47.8771 C 47.732 47.933 48.0797 48.1678 48.2793 48.5138 L 55.6455 61.2863 C 56.0377 61.9663 56.0374 62.8038 55.6448 63.4836 C 55.2521 64.1633 54.5267 64.582 53.7417 64.582 L 14.6233 64.582 C 13.9261 64.582 13.2818 64.2101 12.9331 63.6064 C 12.5843 63.0026 12.5841 62.2588 12.9324 61.6548 L 20.499 48.5348 C 20.7014 48.1837 21.0543 47.9454 21.4556 47.8887 C 21.8569 47.832 22.262 47.9633 22.5538 48.2446 Z",
		lefteye: [19.038, 30.584],
		righteye: [40.6, 30.584]
	},
	B: {
		head: "m 34.71261,5.3035522 c -1.582207,-0.00714 -3.153793,0.5907371 -4.32325,1.6458944 -2.809873,2.3431476 -2.831101,7.2958954 0.201021,9.4474854 -1.164166,1.383505 -2.328066,2.769213 -3.494877,4.152718 -1.661582,2.10661 -3.627687,3.987616 -5.016748,6.291606 -3.664476,6.225639 -2.251392,14.896778 3.386873,19.515604 4.706933,4.144959 12.226029,4.547634 17.345711,0.904855 5.789078,-3.850478 8.101932,-11.76196 5.628081,-18.161166 -0.377916,-0.704551 -0.805976,-1.384543 -1.245402,-2.058789 -0.785835,-1.205765 -2.24531,-1.370323 -3.270602,-0.360184 -1.777099,1.750835 -3.553532,3.502251 -5.331974,5.252909 -0.645623,0.635536 -1.692695,0.634985 -2.337842,-0.001 -0.4955,-0.48849 -0.991244,-0.977085 -1.487248,-1.465543 -0.489832,-0.482381 -0.490427,-1.264355 -0.001,-1.74718 1.814642,-1.790287 3.630139,-3.579657 5.445145,-5.368665 1.362124,-1.342612 1.469276,-3.618564 0.237712,-5.08186 -0.50709,-0.602505 -1.014944,-1.205086 -1.523421,-1.807642 0.873124,-0.883707 2.037312,-2.362073 2.137854,-3.863847 0.703791,-3.6927857 -2.563816,-7.420045 -6.35,-7.2951618 z",
		body: "M 21.683431,49.049202 10.781254,60.133797 a 1.4161369,1.4161369 0 0 0 1.009758,2.409155 h 45.468542 a 1.5943981,1.5943981 0 0 0 1.136365,-2.711979 L 47.792432,49.049719 46.538245,50.258431 c -3.368143,2.659324 -7.429401,4.212146 -11.800314,4.212146 -4.370912,0 -8.432171,-1.552822 -11.800313,-4.212146 z",
		lefteye: [19.165, 34.246],
		righteye: [35.963, 34.246]
	},
	R: {
		head: "m 13.753224,7.9527927 h 5.286458 a 3.3689508,3.3689508 0 0 1 3.368951,3.368951 v 5.763157 a 2.4121423,2.4121423 0 0 0 2.412143,2.412142 h 0.825082 a 2.4121423,2.4121423 0 0 0 2.412141,-2.412142 v -5.763157 a 3.3689508,3.3689508 0 0 1 3.368951,-3.368951 h 4.560843 a 3.3689508,3.3689508 0 0 1 3.36895,3.368951 v 5.763157 a 2.4121423,2.4121423 0 0 0 2.412143,2.412142 h 0.825082 a 2.4121423,2.4121423 0 0 0 2.412142,-2.412142 v -5.763157 a 3.3689508,3.3689508 0 0 1 3.368951,-3.368951 h 5.286458 a 3.3689508,3.3689508 0 0 1 3.368952,3.368951 v 18.897072 a 3.9213113,3.9213113 0 0 1 -3.921312,3.921311 h -38.803575 a 3.9213113,3.9213113 0 0 1 -3.921311,-3.921311 V 30.033549 a 3.3689508,3.3689508 0 0 1 3.368951,-3.368951 z",
		body: "M 16.187943,59.668882 19.08111,41.287407 a 3.4034341,3.4034341 0 0 1 3.362044,-2.874263 h 22.528435 a 3.4034341,3.4034341 0 0 1 3.362044,2.874263 L 51.2268,59.668882 a 2.4875017,2.4875017 0 0 1 -2.457251,2.874262 H 18.645194 a 2.4875017,2.4875017 0 0 1 -2.457251,-2.874262 z ",
		lefteye: [18.446, 21.773],
		righteye: [39.979, 21.773]
	},
	P: {
		head: "M 44.5952 27.5546 C 44.5952 33.534 39.748 38.3812 33.7686 38.3812 C 27.7893 38.3812 22.9421 33.534 22.9421 27.5546 C 22.9421 21.5753 27.7893 16.7281 33.7686 16.7281 C 39.748 16.7281 44.5952 21.5753 44.5952 27.5546 Z",
		body: "m 20.850384,58.642671 2.54562,-13.44348 a 4.0627153,4.0627153 0 0 1 3.99178,-3.30685 h 12.76166 a 4.0627153,4.0627153 0 0 1 3.99178,3.30685 l 2.54562,13.44348 a 2.7881143,2.7881143 0 0 1 -2.73944,3.30684 h -20.35758 a 2.7881143,2.7881143 0 0 1 -2.73944,-3.30684 z",
		lefteye: [21.391, 23.060],
		righteye: [35.605, 23.060]
	},
	Q: {
		head: "M 13.9911 11.5225 L 23.1338 21.2289 C 23.9832 22.393 25.2753 23.155 26.7051 23.3349 L 32.6455 9.76374 C 32.8464 9.30475 33.3004 9.00861 33.8014 9.00972 C 34.3024 9.01083 34.7551 9.30897 34.954 9.76884 L 39.672 20.6794 C 39.9728 22.0309 41.0479 23.0745 42.4078 23.3349 L 53.4411 11.9545 C 53.7514 11.6345 54.2283 11.542 54.6357 11.7229 C 55.043 11.9037 55.2943 12.3195 55.265 12.7642 L 54.0574 31.1203 C 53.9525 32.7147 52.6055 34.3075 51.027 34.552 C 48.3923 34.9601 43.8577 35.4674 37.1457 35.5841 C 35.5482 35.6119 32.9574 35.6123 31.36 35.5782 C 24.1318 35.4239 18.9499 34.7175 16.0132 34.1915 C 14.442 33.9101 13.1223 32.3015 13.0445 30.7055 L 12.1481 12.3062 C 12.1264 11.8597 12.3851 11.447 12.7965 11.2721 C 13.2078 11.0971 13.6846 11.1971 13.991 11.5224 Z ",
		body: "M 15.9586 59.2067 L 18.8932 40.5624 C 19.1316 39.0475 20.5618 37.9442 22.0886 38.0867 C 32.2882 39.0381 37.2774 39.0654 44.9178 38.1666 C 46.4408 37.9874 47.8659 39.0475 48.1043 40.5624 L 51.0389 59.2067 C 51.1468 59.8923 50.9492 60.5908 50.4982 61.1183 C 50.0473 61.6458 49.388 61.9495 48.694 61.9495 L 18.3035 61.9495 C 17.6095 61.9495 16.9502 61.6458 16.4992 61.1183 C 16.0483 60.5908 15.8507 59.8923 15.9586 59.2067 Z ",
		lefteye: [17.525, 24.205],
		righteye: [40.882, 24.205]
	},
	S: {
		head: "M 0,0 H 67.5 V 67.5 H 0 Z",
		body: "M 22.24",
		lefteye: [400000, 400000],
		righteye: [400000, 400000],
		disableEyes: true
	}
	
}

function elnorm(arr, order = 2, length = undefined){
	if (length === undefined){
		length = arr.length;
	}

	if (order === 0) {
		let total = 0.;
		for (let i = 0; i < length; i++) {
			if (arr[i] != 0) {
				total += 1;
			}
		}
		return total
	}

	if (order === 1) {
		let total = 0;
		for (let i = 0; i < length; i++) {
			total += Math.abs(arr[i]);
		}
		return total
	}

	let total = 0;
	for (let i = 0; i < length; i++) {
		total += Math.pow(Math.abs(arr[i]), order);
	}
	return Math.pow(total, 1/order);

}

class Matrix {
	constructor(width, height, ident = true) {
		if (height === undefined)
			height = width;
		this.width = width;
		this.height = height;
		this.values = new Float64Array(width * height);
		if (ident) {
			for (let i = 0; i < Math.min(width,height); i++){
				this.values[i + this.width * i] = 1;
			}
		} else {
		}
	}

	matMul(other){
		if (this.width != other.height) {
			return;
		}

		let out = new Matrix(other.width, this.height, false);
		for (let i = 0; i < this.height; i++)
			for (let j = 0; j < other.width; j++)
				for (let k = 0; k < this.width; k++)
					out.values[i * other.width + j] += this.values[i * this.width + k] * other.values[k * other.width + j];
		return out;
	}

	transformVec(vec){
		let out = new Float64Array(vec.length);
		for (let i = 0; i < this.height; i++)
			for (let k = 0; k < this.width; k++)
				out.values[i * other.width] += this.values[i * this.width + k] * other[k * other.width];

		return out;
	}

	scaMul(constant) {
		for (let i = 0; i < this.values.length; i++)
			this.values[i] = this.values[i] * constant;
	}

	add(other){
		let out = new Matrix(other.width, this.height, false);
		for (let i = 0; i < this.values.length; i++)
			out.values[i] = this.values[i] + other.values[i];
		return out;
	}

	addTo(other){
		for (let i = 0; i < this.values.length; i++)
			this.values[i] += other.values[i];
		return this;
	}

	cloned(){
		let out = new Matrix(this.width, this.height, false);
		out.values = this.values.slice(0);
		return out;
	}

	lerp(other, val) {
		let c1 = this.cloned();
		c1.scaMul(1 - val);
		let c2 = other.cloned();
		c2.scaMul(val);
		c1.addTo(c2);
		return c1;
	}

	static Translation(...args) {
		let out = new Matrix(args.length);
		let outH = Matrix.Homogenize(out);
		for (let i = 0; i < args.length; i ++)
			outH.values[i * outH.width + outH.width - 1] = args[i];
		return outH;
	}

	static Scale(...args) {
		let out = new Matrix(args.length, args.length, false);
		for (let i = 0; i < args.length; i ++)
			out.values[i * out.width + i] = args[i];
		return out;
	}

	static Rotation(...args) {
		let x = args[0];

		let [sinx, cosx] = [Math.sin(x), Math.cos(x)];
		if (args.length === 1) {
			let out = new Matrix(2, 2, false);
			out.values = new Float64Array([
				cosx, -sinx,
				sinx, cosx
			]);
			return out;
		}
		let y = args.length > 1 ? args[1] : 0.;
		let z = args.length > 2 ? args[2] : 0.;
		let [siny, cosy, sinz, cosz] = [Math.sin(y), Math.cos(y), Math.sin(z), Math.cos(z)];
		let out = new Matrix(3, 3, false);
		out.values = new Float64Array([cosx * cosy, cosx * siny * sinz - sinx * cosz, cosx * siny * cosz + sinx * sinz,
							 sinx * cosy, sinx * siny * sinz + cosx * cosz, sinx * siny * cosz - cosx * sinz,
							 -siny, cosy * sinz, cosy * cosz]);
		return out;
	}

	static Homogenize(mat) {
		const BYTES_PER_ELEMENT = mat.values.BYTES_PER_ELEMENT;
		let out = new Matrix(mat.width + 1, mat.height + 1, false);
		let from = mat.values.buffer;
		for (let i = 0; i < mat.height; i ++){
			out.values.set(mat.row(i), i * out.width);
		}
		out.values[mat.height * out.width + mat.width] = 1;

		return out;
	}

	static RotateAround(rotation, center) {
		return Matrix.Translation(...center)
			.matMul(Matrix.Homogenize(Matrix.Rotation(...rotation)))
			.matMul(Matrix.Translation(...center.map(x => -x)));
	}

	static ScaleFrom(scale, center){
		return Matrix.Translation(...center)
			.matMul(Matrix.Homogenize(Matrix.Scale(...scale))
			.matMul(Matrix.Translation(...center.map(x => -x))));
	}

	/**
	 * @param {number} order - order of the norm
	 * @param {number} type_- 0 for elementwise, 1 for row wise, 2 for columnwise, ..., -1 for svd
	 */
	fnorm(order = 1, type_ = 0){
		if (type_ === 0) {
			return elnorm(this.values, order, this.width * this.height);
		}
		console.error("not implemented yet");
	}

	row(i) {
		return new Float64Array(this.values.buffer, this.values.BYTES_PER_ELEMENT * i * this.width, this.width);
	}

	column(i) {
		let out = new Float64Array(this.height);
		for (let j = 0; j < this.height; j ++)
			out[j] = this.values[j * this.width + i];
		return out;
	}

	svd2d() {
		if (this.width != 2 || this.height != 2) {
			console.error("not implemented yet");
		}

		let a00 = this.values[0] * this.values[0] + this.values[1] * this.values[1];
		let a01 = this.values[0] * this.values[2] + this.values[1] * this.values[3];
		let a11 = this.values[2] * this.values[2] + this.values[3] * this.values[3];
		let a00p= this.values[0] * this.values[0] + this.values[2] * this.values[2];
		let a01p= this.values[0] * this.values[1] + this.values[2] * this.values[3];
		let a11p= this.values[1] * this.values[1] + this.values[3] * this.values[3];

		let phix = a00 - a11;
		let phi = Math.atan2(2 * a01, phix) / 2;
		let theta = Math.atan2(2 * a01p, a00p - a11p) / 2;

		let asum = a00 + a11;
		let adif = phix + 4 * a01 * a01;
		let scale1 = Math.sqrt((asum + adif) / 2)
		let scale2 = Math.sqrt((asum - adif) / 2)

		return [phi, theta, scale1, scale2];
	}


	toCanvas2d(){
		return [this.values[0], this.values[3], this.values[1], this.values[4], this.values[2], this.values[5]];
	}
}

let globalTransform = Matrix.Translation(100, 100).matMul(Matrix.ScaleFrom([2.5, 2.5], [0., 0.]));
let globalPieceStyle = {
	bgColor1: "#EDC55A",
	bgColor2: "#3c4b4b00",
	fgColor1: "#fff",
	fgColor2: "#262626",
	eyeRadius: 5,
}
let eyeContour = "#262626"

class ChessPiece {
	constructor(type_, style, side = 0){
		if (style === undefined)
			style = {};
		if (type_.length == 2){
			side = ~~(type_[0] == "w");
			type_ = type_[1];
		}
		this.type_ = type_;
		this.side = side;
		this.transformFrom = {
			main: new Matrix(3),
			head: new Matrix(3),
			body: new Matrix(3),
			leftEye: new Matrix(3),
			rightEye: new Matrix(3)
		}
		this.transformTo = {
			main: new Matrix(3),
			head: new Matrix(3),
			body: new Matrix(3),
			leftEye: new Matrix(3),
			rightEye: new Matrix(3)
		}
		this.transform = {
			main: new Matrix(3),
			head: new Matrix(3),
			body: new Matrix(3),
			leftEye: new Matrix(3),
			rightEye: new Matrix(3)
		}
		this.style = {__proto__: globalPieceStyle, ...style}
		this.t_ = 0; // time value
	}

	setTransformInit(trafo, part = "main") {
		this.transformFrom[part] = trafo;
		this.transform[part] = trafo;
	}

	transformInit(trafo, part = "main") {
		this.transformFrom[part] = trafo.matMul(this.transformFrom[part]);
		this.transform[part] = trafo.matMul(this.transform[part]);
	}

	setTransformAll(trafo, part = "main") {
		this.setTransformInit(trafo, part);
		this.transformTo[part] = trafo;
	}

	transformAll(trafo, part = "main") {
		this.transformInit(trafo, part);
		this.transformTo[part] = trafo.matMul(this.transformTo[part]);
	}


	draw(ctx){
		let data = pieceData[this.type_];
		ctx.fillStyle = this.type_ == "S" ? [this.style.bgColor1, this.style.bgColor2][this.side]
			: [this.style.fgColor1, this.style.fgColor2][this.side];
		ctx.setTransform(...globalTransform.toCanvas2d());
		ctx.transform(...this.transform.main.toCanvas2d());
		ctx.transform(...this.transform.body.toCanvas2d());
		ctx.fill(new Path2D(data.body));
		ctx.setTransform(...globalTransform.toCanvas2d());
		ctx.transform(...this.transform.main.toCanvas2d());
		ctx.transform(...this.transform.head.toCanvas2d());
		ctx.fill(new Path2D(data.head));
		if ("disableEyes" in data && data["disableEyes"]) {
			return;
		}
		ctx.strokeStyle = eyeContour;
		ctx.fillStyle = "#FFFFFF";
		ctx.setTransform(...globalTransform.toCanvas2d());
		ctx.transform(...this.transform.main.toCanvas2d());
		ctx.transform(...this.transform.leftEye.toCanvas2d());
		ctx.beginPath();
		ctx.lineWidth = 1.75;
		ctx.arc(data.lefteye[0] + this.style.eyeRadius, data.lefteye[1] + this.style.eyeRadius, this.style.eyeRadius, 0, 2 * Math.PI);
		ctx.fill();
		ctx.stroke();
		ctx.setTransform(...globalTransform.toCanvas2d());
		ctx.transform(...this.transform.main.toCanvas2d());
		ctx.transform(...this.transform.rightEye.toCanvas2d());
		ctx.beginPath();
		ctx.arc(data.righteye[0] + this.style.eyeRadius, data.righteye[1] + this.style.eyeRadius, this.style.eyeRadius, 0, 2 * Math.PI);
		ctx.fill();
		ctx.stroke();
	}

	set t(t) {
		this.t_ = t;
		this.transform = {
			main: this.transformFrom.main.lerp(this.transformTo.main, t),
			head: this.transformFrom.head.lerp(this.transformTo.head, t),
			body: this.transformFrom.body.lerp(this.transformTo.body, t),
			leftEye: this.transformFrom.leftEye.lerp(this.transformTo.leftEye, t),
			rightEye: this.transformFrom.rightEye.lerp(this.transformTo.rightEye, t),
		}
	}

	get t(){
		return this.t_;
	}

	getTranslate() {
		return [this.transform.main.values[2], this.transform.main.values[5]];
	}
	
	getCenter() {
		let a = this.getTranslate();
		let s = squareWidth / 2;
		return [a[0] + s, a[1] + s];
	}
}


class DrawableCollection {
	constructor(elements){
		this.elements = elements ?? [];
	}

	/**
     * @param {number} t
     */
	set t(t) {
		this.elements.forEach(e => e.t = t);
	}

	draw(ctx) {
		this.elements.forEach(e => e.draw(ctx));
	}
}

let fgCanvas = document.getElementById("foreground-canvas");
let fgCtx = fgCanvas.getContext("2d");

let bgCanvas = document.getElementById("background-canvas");
let bgCtx = bgCanvas.getContext("2d");

let testSubject = new ChessPiece("bK");
testSubject.transformTo.body = Matrix.ScaleFrom([2, 2], [50, 50]);

let counter = 0, lastStamp = 0;


let initConfig = [
	["bR", "bN", "bB", "bQ", "bK", "bB", "bN", "bR"],
	["bP", "bP", "bP", "bP", "bP", "bP", "bP", "bP"],
	[" ", " ", " ", " ", " ", " ", " ", " "],
	[" ", " ", " ", " ", " ", " ", " ", " "],
	[" ", " ", " ", " ", " ", " ", " ", " "],
	[" ", " ", " ", " ", " ", " ", " ", " "],
	["wP", "wP", "wP", "wP", "wP", "wP", "wP", "wP"],
	["wR", "wN", "wB", "wQ", "wK", "wB", "wN", "wR"],
];

let squareWidth = 67.5;
let figures = new DrawableCollection(
	initConfig.map((x, i) => x.filter(y => y != " ").map((y, j) => {
		let piece = new ChessPiece(y);
		piece.setTransformAll(Matrix.Translation(j * squareWidth, (7 - i) * squareWidth));


		let scaleR = Math.random() * 3 + 1;
		piece.transformInit(Matrix.ScaleFrom([scaleR, scaleR], piece.getCenter()));
		piece.transformInit(Matrix.RotateAround([2 * Math.PI * Math.random()], piece.getCenter()));
		piece.transformInit(Matrix.Translation(1800 * Math.random() - 900, 1200 * Math.random()- 600));

		// eye balling randomness,. .., HA!
		let a = 20 * (Math.random() - .5);
		let b = 20 * (Math.random() - .5);
		piece.setTransformAll(Matrix.Translation(a, b), "leftEye");
		piece.setTransformAll(Matrix.Translation(a + 10 * (Math.random() - .5), b + 10 * (Math.random() - .5)), "rightEye");
		return piece;
	})).flat()
);

let squares = new DrawableCollection();

for (let i = 0; i < 8; i++)
	for (let j = 0; j < 8; j++) {
		let square = new ChessPiece("S", {}, (i + j)%2);
		square.transformTo["main"] = Matrix.Translation(i * squareWidth, j * squareWidth);


		let scaleR = Math.random() * 3 + 1;
		square.transformInit(Matrix.ScaleFrom([scaleR, scaleR], square.getCenter()));
		square.transformInit(Matrix.RotateAround([2 * Math.PI * Math.random()], square.getCenter()));
		square.transformInit(Matrix.Translation(1800 * Math.random() - 900, 1200 * Math.random()- 600));

		squares.elements.push(square);
	}



console.log(squares.elements);


let globalT = 0;
function singleStep(timeStamp) {
	let deltaTime = timeStamp - lastStamp;
	counter += 1;
	fgCtx.setTransform(1, 0, 0, 1, 0, 0);
	fgCtx.clearRect(0, 0, fgCanvas.width, fgCanvas.height);
	bgCtx.setTransform(1, 0, 0, 1, 0, 0);
	bgCtx.clearRect(0, 0, bgCanvas.width, bgCanvas.height);

	squares.t = globalT;
	squares.draw(bgCtx);
	figures.t = globalT;
	figures.draw(bgCtx);

	lastStamp = timeStamp;
}
function step(timeStamp){
	singleStep(timeStamp);
	requestAnimationFrame(step);
}


window.mobileCheck = function() {
	let check = false;
	(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
	return check;
};

if (window.mobileCheck()) {
	console.log("hi");
	document.body.style.setProperty('--main-fixed', 'static');
	document.body.style.setProperty('--main-padding', '15px');
	document.body.style.setProperty('--main-bg-display', 'none');
	document.body.style.setProperty('--font-size', '9pt');
	singleStep(0.);
} else {
	window.addEventListener('scroll', () => {
		// let totalHeight = document.getElementById("main-filler").getBoundingClientRect().height;
		let totalHeight = window.innerHeight * 2.5;
		let scrollRelative = Math.min(1, window.scrollY / (totalHeight - window.innerHeight));
		document.body.style.setProperty('--scrollPosition', window.scrollY);
		// document.body.style.setProperty('--scrollRelative', Math.min(1, scrollRelative * 2));
		document.body.style.setProperty('--scrollRelative', scrollRelative);

		globalT = scrollRelative;

		document.querySelectorAll(".scroll-to-none").forEach(x => {
			if (window.getComputedStyle(x).opacity < 0.02 || window.innerWidth < 1250) {
				x.style.display = "none";
			} else {
				x.style.display = "block";
			}
		})
	}, false);
	requestAnimationFrame(step);
}






function handleScreenSize(){
	fgCanvas.width = window.innerWidth * 2;
	fgCanvas.height = window.innerHeight * 2;

	bgCanvas.width = window.innerWidth * 2;
	bgCanvas.height = window.innerHeight * 2;
	let scale = Matrix.ScaleFrom([2.7, 2.7], [0., 0.]);
	globalTransform = Matrix.Translation(fgCanvas.width / 2, fgCanvas.height / 2 )
		.matMul(scale)
		.matMul(Matrix.Translation(-squareWidth * 4, -squareWidth * 4));
}

window.addEventListener("resize", handleScreenSize);
handleScreenSize()

let integrations = document.getElementById("integrations");
let instaElement = document.getElementById("instagram-allow-banner");
let discordElement = document.getElementById("discord-allow-banner");
let integrationScript = document.getElementById("integration-script");
instaElement.addEventListener("mouseup", (ev) => {
	console.log("hi");
	instaElement.innerHTML = `<blockquote class="instagram-media" data-instgrm-captioned data-instgrm-permalink="https://www.instagram.com/p/C7HGA1lodcZ/?utm_source=ig_embed&amp;utm_campaign=loading" data-instgrm-version="14" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:540px; min-width:326px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:16px;"> <a href="https://www.instagram.com/p/C7HGA1lodcZ/?utm_source=ig_embed&amp;utm_campaign=loading" style=" background:#FFFFFF; line-height:0; padding:0 0; text-align:center; text-decoration:none; width:100%;" target="_blank"> <div style=" display: flex; flex-direction: row; align-items: center;"> <div style="background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;"></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;"></div></div></div><div style="padding: 19% 0;"></div> <div style="display:block; height:50px; margin:0 auto 12px; width:50px;"><svg width="50px" height="50px" viewBox="0 0 60 60" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-511.000000, -20.000000)" fill="#000000"><g><path d="M556.869,30.41 C554.814,30.41 553.148,32.076 553.148,34.131 C553.148,36.186 554.814,37.852 556.869,37.852 C558.924,37.852 560.59,36.186 560.59,34.131 C560.59,32.076 558.924,30.41 556.869,30.41 M541,60.657 C535.114,60.657 530.342,55.887 530.342,50 C530.342,44.114 535.114,39.342 541,39.342 C546.887,39.342 551.658,44.114 551.658,50 C551.658,55.887 546.887,60.657 541,60.657 M541,33.886 C532.1,33.886 524.886,41.1 524.886,50 C524.886,58.899 532.1,66.113 541,66.113 C549.9,66.113 557.115,58.899 557.115,50 C557.115,41.1 549.9,33.886 541,33.886 M565.378,62.101 C565.244,65.022 564.756,66.606 564.346,67.663 C563.803,69.06 563.154,70.057 562.106,71.106 C561.058,72.155 560.06,72.803 558.662,73.347 C557.607,73.757 556.021,74.244 553.102,74.378 C549.944,74.521 548.997,74.552 541,74.552 C533.003,74.552 532.056,74.521 528.898,74.378 C525.979,74.244 524.393,73.757 523.338,73.347 C521.94,72.803 520.942,72.155 519.894,71.106 C518.846,70.057 518.197,69.06 517.654,67.663 C517.244,66.606 516.755,65.022 516.623,62.101 C516.479,58.943 516.448,57.996 516.448,50 C516.448,42.003 516.479,41.056 516.623,37.899 C516.755,34.978 517.244,33.391 517.654,32.338 C518.197,30.938 518.846,29.942 519.894,28.894 C520.942,27.846 521.94,27.196 523.338,26.654 C524.393,26.244 525.979,25.756 528.898,25.623 C532.057,25.479 533.004,25.448 541,25.448 C548.997,25.448 549.943,25.479 553.102,25.623 C556.021,25.756 557.607,26.244 558.662,26.654 C560.06,27.196 561.058,27.846 562.106,28.894 C563.154,29.942 563.803,30.938 564.346,32.338 C564.756,33.391 565.244,34.978 565.378,37.899 C565.522,41.056 565.552,42.003 565.552,50 C565.552,57.996 565.522,58.943 565.378,62.101 M570.82,37.631 C570.674,34.438 570.167,32.258 569.425,30.349 C568.659,28.377 567.633,26.702 565.965,25.035 C564.297,23.368 562.623,22.342 560.652,21.575 C558.743,20.834 556.562,20.326 553.369,20.18 C550.169,20.033 549.148,20 541,20 C532.853,20 531.831,20.033 528.631,20.18 C525.438,20.326 523.257,20.834 521.349,21.575 C519.376,22.342 517.703,23.368 516.035,25.035 C514.368,26.702 513.342,28.377 512.574,30.349 C511.834,32.258 511.326,34.438 511.181,37.631 C511.035,40.831 511,41.851 511,50 C511,58.147 511.035,59.17 511.181,62.369 C511.326,65.562 511.834,67.743 512.574,69.651 C513.342,71.625 514.368,73.296 516.035,74.965 C517.703,76.634 519.376,77.658 521.349,78.425 C523.257,79.167 525.438,79.673 528.631,79.82 C531.831,79.965 532.853,80.001 541,80.001 C549.148,80.001 550.169,79.965 553.369,79.82 C556.562,79.673 558.743,79.167 560.652,78.425 C562.623,77.658 564.297,76.634 565.965,74.965 C567.633,73.296 568.659,71.625 569.425,69.651 C570.167,67.743 570.674,65.562 570.82,62.369 C570.966,59.17 571,58.147 571,50 C571,41.851 570.966,40.831 570.82,37.631"></path></g></g></g></svg></div><div style="padding-top: 8px;"> <div style=" color:#3897f0; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:550; line-height:18px;">View this post on Instagram</div></div><div style="padding: 12.5% 0;"></div> <div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;"><div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div> <div style="background-color: #F4F4F4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div></div><div style="margin-left: 8px;"> <div style=" background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div> <div style=" width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg)"></div></div><div style="margin-left: auto;"> <div style=" width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div> <div style=" background-color: #F4F4F4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div> <div style=" width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div></div></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center; margin-bottom: 24px;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 224px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 144px;"></div></div></a><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/C7HGA1lodcZ/?utm_source=ig_embed&amp;utm_campaign=loading" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared by Universität Augsburg Schach (@uni.augsburg.schach)</a></p></div></blockquote>`;
	integrationScript.innerHTML = `<script async src="https://instagram.com/static/bundles/es6/EmbedSDK.js/47c7ec92d91e.js"></script>`;
});

discordElement.addEventListener("mouseup", function(e) {
	discordElement.innerHTML = `<iframe src="https://discord.com/widget?id=1184911351034945536&theme=dark" width="350" height="500" allowtransparency="true" frameborder="0" sandbox="allow-popups allow-popups-to-escape-sandbox allow-same-origin allow-scripts"></iframe>`;
});



console.log(instaElement);

// TODO: maybe switch to a templating engine
let textDE =  {
	"#rules > h1" : "Regeln",
	"#rules > ul > li:nth-child(1)" : "Kein Alkohol",
	"#rules > ul > li:nth-child(2)" : "Räume sauber halten",
	"#rules > ul > li:nth-child(3)" : "Anweisunegn der Koordinatoren einhalten",
	"#rules > ul > li:nth-child(4)" : "Spaß haben!",
	"#slide-1 > span" : "Blitzturniere, die wir organisieren",
	"#slide-2 > span" : "Bild vom 6.6.2024",
	"#slide-3 > span" : "Gewinner des Blitzturniers",
	"#slide-5 > span" : "Der erste Schachabend wird am 18.04.2024 stattfinden! Wir freuen uns darauf dich zu sehen!",
	"#team-list > li:nth-child(1)" : "Hauptkoordinator: Elisabeth Welizky",
	"#team-list > li:nth-child(5)" : "Webseitenbetreiber: Tuan Bui",
	"#integrations > div:nth-child(1) > span" : "Klicke hier, um die Discordintegration zu erlauben",
	"#integrations > div:nth-child(2) > span" : "Klicke hier, um die Instagramintegration zu erlauben",
	"h2.desc" : "Was ist das?",
	"#desc1 > span:nth-of-type(1)" : "Willkommen beim Schachclub der Universität Augsburg!",
	"#desc1 > span:nth-of-type(2)" : "Wir organisieren",
	"#desc1 > b:nth-of-type(1)" : "regelmäßige und kostenlose Schachabende",
	"#desc1 > span:nth-of-type(3)" : "wo Studenten aus allen Fachrichtungen willkommen sind. Egal ob",
	"#desc1 > b:nth-of-type(2)" : "Anfänger oder Fortgeschrittene",
	"#desc1 > span:nth-of-type(4)" : ": Du wirst herausgefordert und hast eine Chance dein Schachwissen unter Beweis zu stellen und neue Freunde kennenzulernen.",
	"#desc1 > span:nth-of-type(5)" : "Wir stellen Schachbretter bereit. Wenn du aber ein Schachbrett besitzt, kannst du dieses gerne mitbringen.",
	"#desc1 > span:nth-of-type(6)" : "Beachte: Wir bieten keine Schachanalysen an.",
	"#desc1 > span:nth-of-type(7)" : "Wir freuen uns dich zu sehen!",
	"#when-question": "Wann und wo findet es statt?",
	"#text1": "Jeden Donnerstag um 18:00-21:00 in den Räumen D 1011 und D 1012 an der Universität Augsburg:",
	"#socmed > h2": "Folgt uns auf Social Media!",
	"#short-where > b" : "Wo?",
	"#short-when > b" : "Wann?",
	"#short-when > span" : "1. & 3. Do., 18:00 - 21:00",
}
let textEN = {}

function initializeDefaultLanguage(){
	for (let key in textDE) {
		textEN[key] = document.querySelector(key).innerText;
	}
}
initializeDefaultLanguage();

let currentLang = "en";
let langElem = {
	"de": document.querySelector(".language-de"),
	"en": document.querySelector(".language-en")
};

function translate(lang) {
	if (lang === currentLang) {
		return
	}

	langElem[currentLang].classList.remove("selected");
	let dictionary = textEN;
	if (lang === "de") {
		dictionary = textDE;
	}

	for (let key in dictionary) {
		document.querySelector(key).innerText = dictionary[key];
	}
	currentLang = lang;
	langElem[currentLang].classList.add("selected");
}

langElem["de"].addEventListener("click", () => translate("de"));
langElem["en"].addEventListener("click", () => translate("en"));
