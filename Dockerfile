FROM debian:latest

RUN apt-get update && apt-get install -y openssh-server curl git tmux build-essential
RUN mkdir /var/run/sshd
RUN echo 'root:root123' | chpasswd # change it as soon as possible!!!
RUN sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config
RUN useradd -m web-server
USER web-server
WORKDIR /home/web-server

RUN git clone https://codeberg.org/ekstdo/schachwebsite.git
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | bash -s -- -y

WORKDIR /home/web-server/schachwebsite

RUN /home/web-server/.cargo/bin/cargo build

EXPOSE 22
EXPOSE 8000

USER root
WORKDIR /home/web-server/schachwebsite

ENTRYPOINT ["/bin/sh", "-c", "(/usr/sbin/sshd -D &) && runuser -l web-server -c \"cd /home/web-server/schachwebsite && ROCKET_ENV=release /home/web-server/.cargo/bin/cargo run --release\""]

