#[macro_use] extern crate rocket;
use std::path::{Path, PathBuf};
use rocket::fs::NamedFile;
use rocket::fs::FileServer;

#[get("/")]
async fn index()  -> Option<NamedFile>  {
    NamedFile::open(Path::new("static/Schachwebsite.html")).await.ok()
}

#[launch]
fn rocket() -> _ {
    rocket::build()
        .mount("/", routes![index])
         // serve files from `/www/static` at path `/public`
        .mount("/static", FileServer::from("./static"))
}
